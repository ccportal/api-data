import flask
from flask import request, jsonify
from pymongo import MongoClient
from datahub_api import datahub

client = MongoClient("localhost", 27017)

topics = client.test.topics


app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route("/", methods=["GET"])
def home():
    return jsonify(dict())


@app.route("/post_data", methods=["POST"])
def post_data():
    """Adding data to an existing point
    ---
    parameters:
    - name: topic_name
      in: json
      type: string
      required: true
      description: name of the topic
    - name: question_content
      in: json
      type: string
      required: true
      description: involved question
    - name: point_name
      in: json
      type: string
      required: true
      description: name of the point
    - name: data
      in: json
      type: object
      required: true
      description: data to include with keys labels, type, title, datasets. datasets is a list of objects with keys label, data, borderColor
    responses:
      200:
        description: Successful insert
    
    """
    print(list(topics.find()))
    arg = request.get_json()
    topic_name = arg.get("topic_name")
    question_content = arg.get("question_content")
    point_name = arg.get("point_name")
    data = arg.get("data")

    topics.update_one(
        dict(name=topic_name),
        {"$set": {"questions.$[element1].points.$[element2].data": data}},
        array_filters=[
            {"element1.content": question_content},
            {"element2.name": point_name},
        ],
        upsert=True,
    )
    return jsonify({"message": "successful!"})


app.register_blueprint(datahub)

if __name__ == "__main__":
    app.run()
