from flask import Blueprint, jsonify
import pandas as pd

datahub = Blueprint("datahub", __name__)


@datahub.route("/globaltemp", methods=["GET"])
def get_global_temperatures():
    df = pd.read_csv(
        "https://data.giss.nasa.gov/gistemp/graphs_v4/graph_data/Temperature_Anomalies_over_Land_and_over_Ocean/graph.csv",
        header=1,
    )
    df = df.iloc[:, [0, 2, 4]]
    df.columns = ["year", "land_annual", "ocean_annual"]
    return jsonify(df.to_dict(orient="list"))
