import requests

url = "http://localhost:5000"


def add_global_temp():
    global_temperatures = requests.get(url + "/globaltemp").json()

    json_body = {
        "topic_name": "État actuel du climat ⏱",
        "question_content": "L'influence humaine a-t-elle provoqué un réchauffement rapide et généralisé de l'atmosphère, des océans et de la Terre ?",
        "point_name": "Oui : on observe un réchauffement exceptionnel 🌡📈",
        "data": {
            "labels": global_temperatures["year"],
            "type": "line",
            "title": "Anomalie de température depuis 1880 (NASA GISS)",
            "datasets": [
                {
                    "label": "sur la terre",
                    "data": global_temperatures["land_annual"],
                    "borderColor": "green",
                },
                {
                    "label": "dans les océans",
                    "data": global_temperatures["ocean_annual"],
                    "borderColor": "blue",
                },
            ],
        },
    }
    response = requests.post(url + "/post_data", json=json_body)
    assert response.ok


if __name__ == "__main__":
    add_global_temp()
